package clases;

import java.util.ArrayList;

public class GestorContabilidad {

	private ArrayList<Factura> listaFacturas;
	private ArrayList<Cliente> listaClientes;

	public GestorContabilidad() {

		listaFacturas = new ArrayList<Factura>();
		listaClientes = new ArrayList<Cliente>();
	}

	public ArrayList<Factura> getListaFacturas() {
		return listaFacturas;
	}

	public void setListaFacturas(ArrayList<Factura> listaFacturas) {
		this.listaFacturas = listaFacturas;
	}

	public ArrayList<Cliente> getListaClientes() {
		return listaClientes;
	}

	public void setListaClientes(ArrayList<Cliente> listaClientes) {
		this.listaClientes = listaClientes;
	}

	public Cliente buscarCliente(String dni) {

		for (Cliente cliente : listaClientes) {

			if (cliente.getDni().equals(dni)) {

				return cliente;

			}

		}
		return null;

	}

	public Factura buscarFactura(String codigo) {

		for (Factura factura : listaFacturas) {

			if (factura.getCodigoFactura().equals(codigo)) {

				return factura;

			}

		}
		return null;

	}

	public void altaCliente(Cliente cliente) {

		if (!listaClientes.contains(cliente)) {

			listaClientes.add(cliente);

		}

	}

	public void crearFactura(Factura factura) {

		if (!listaFacturas.contains(factura)) {

			listaFacturas.add(factura);

		}

	}

	public Cliente clienteMasAntiguo() {

		if (listaClientes.size() > 0) {
			Cliente clienteAntiguo = listaClientes.get(0);
			if (clienteAntiguo != null) {
				for (Cliente cliente : listaClientes) {
					if (clienteAntiguo.getFechaAlta().isAfter(cliente.getFechaAlta())) {
						clienteAntiguo = cliente;
					}
				}
				return clienteAntiguo;
			} else {
				return null;
			}
		} else {
			return null;
		}

	}

	public Factura facturaMasCara() {

		if (listaFacturas.size() > 0) {
			Factura facturaCara = listaFacturas.get(0);
			if (facturaCara != null) {
				for (Factura factura : listaFacturas) {
					if (facturaCara.calcularPrecioTotal() < factura.calcularPrecioTotal()) {
						facturaCara = factura;
					}
				}
				return facturaCara;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public float calcularFacturacionAnual(int anno) {

		float facturacionAnual = 0;
		for (Factura factura : listaFacturas) {
			if (factura.getFecha().getYear() == anno) {
				facturacionAnual += factura.calcularPrecioTotal();
			}
		}
		return facturacionAnual;

	}

	public void asignarClienteAFactura(String dni, String codigoFactura) {

		for (Factura factura : listaFacturas) {
			if (factura.getCodigoFactura().equals(codigoFactura)) {
				for (Cliente cliente : listaClientes) {
					if (cliente.getDni().equals(dni)) {

						factura.setCliente(cliente);

					}

				}

			}

		}

	}

	public int cantidadFacturasPorCliente(String dni) {

		int contador = 0;
		for (Factura factura : listaFacturas) {
			if (dni.equals(factura.getCliente().getDni())) {

				contador++;

			}

		}

		return contador;

	}

	public void eliminarFactura(String codigo) {

		for (Factura factura : listaFacturas) {
			if (factura.getCodigoFactura().equals(codigo)) {

				listaFacturas.remove(factura);

			}

		}

	}

	public void eliminarCliente(String dni) {

		for (Cliente cliente : listaClientes) {

			if (cliente.getDni().equals(dni)) {
				listaClientes.remove(cliente);

			}

		}

	}

}
