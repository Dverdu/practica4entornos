package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.GestorContabilidad;

class ClienteTest {
	private GestorContabilidad gestor;

	@BeforeEach
	void setUp() {

		gestor = new GestorContabilidad();
		Cliente cliente = new Cliente("pepito", "43434344", LocalDate.now());
		Cliente cliente2 = new Cliente("juan", "42323232", LocalDate.now());
		gestor.altaCliente(cliente);
		gestor.altaCliente(cliente2);

	}

	// Buscar cliente que existe
	@Test
	void buscarClienteExisteTest() {

		Cliente cliente1 = gestor.buscarCliente("43434344");
		assertNotNull(cliente1);

	}

	// Buscar cliente que no existe
	@Test
	void buscarClienteNoExisteTest() {

		Cliente cliente1 = gestor.buscarCliente("454353453");
		assertNull(cliente1);

	}

	// Alta Cliente repetido
	@Test
	void altaClienteRepetido() {

		Cliente cliente1 = new Cliente("pepito", "43434344", LocalDate.now());
		gestor.altaCliente(cliente1);
		assertEquals(2, gestor.getListaClientes().size());

	}

	// Alta Cliente nuevo
	@Test
	void altaClienteNuevo() {

		Cliente cliente1 = new Cliente("juan", "44324324", LocalDate.now());
		gestor.altaCliente(cliente1);
		assertEquals(3, gestor.getListaClientes().size());

	}

	// Cliente m�s antiguo
	@Test
	void clienteMasAntiguo() {

		Cliente cliente1 = new Cliente("javier", "dfdfdf", LocalDate.of(1993, 12, 12));
		gestor.altaCliente(cliente1);
		assertEquals(null, cliente1, gestor.clienteMasAntiguo());

	}

	// Clientes igual de antiguos
	@Test
	void clientesIgualAntiguos() {

		Cliente cliente1 = new Cliente("paco", "adsasdsa", LocalDate.of(1993, 12, 12));
		Cliente cliente2 = gestor.clienteMasAntiguo();
		gestor.altaCliente(cliente1);
		assertEquals(null, cliente1, cliente2);

	}

	// Eliminar cliente
	@Test
	void eliminarCliente() {

		gestor.eliminarCliente("43434344");
		assertEquals(1, gestor.getListaClientes().size());

	}

	// Eliminar cliente inexistente
	@Test
	void eliminarFacturaInexistente() {

		gestor.eliminarCliente("fdsfsf");
		assertEquals(2, gestor.getListaClientes().size());

	}

}
