package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class FacturaTest {

	private GestorContabilidad gestor;

	@BeforeEach
	void setUp() {
		GestorContabilidad gestor = new GestorContabilidad();
		Cliente cliente = new Cliente("pepito", "43434344", LocalDate.now());
		Cliente cliente2 = new Cliente("juan", "42323232", LocalDate.now());
		Factura factura = new Factura("2324224", LocalDate.now(), "coche", 2000, 1, cliente);
		Factura factura2 = new Factura("432423523", LocalDate.now(), "moto", 600, 1, cliente2);
		gestor.altaCliente(cliente);
		gestor.altaCliente(cliente2);
		gestor.crearFactura(factura);
		gestor.crearFactura(factura2);
	}

	// Buscar Factura que existe
	@Test
	void buscarFacturaExisteTest() {

		Factura factura1 = gestor.buscarFactura("2324224");
		assertNotNull(factura1);

	}

	// Buscar factura que no existe
	@Test
	void buscarFacturaNoExisteTest() {

		Factura factura1 = gestor.buscarFactura("534543534");
		assertNull(factura1);

	}

	// Alta Factura repetida
	@Test
	void altaFacturaRepetida() {

		Cliente cliente1 = new Cliente("pepito", "43434344", LocalDate.now());
		Factura factura1 = new Factura("2324224", LocalDate.now(), "coche", 2000, 1, cliente1);
		gestor.crearFactura(factura1);
		assertEquals(2, gestor.getListaFacturas().size());

	}

	// Alta Factura nueva
	@Test
	void altaFacturaNueva() {

		Cliente cliente1 = new Cliente("juan", "42342342356", LocalDate.now());
		Factura factura1 = new Factura("53453453", LocalDate.now(), "bici", 400, 3, cliente1);
		gestor.crearFactura(factura1);
		assertEquals(3, gestor.getListaClientes().size());

	}

	// Factura mas cara
	@Test
	void facturaCara() {

		Cliente cliente1 = new Cliente("juan", "gedgete", LocalDate.now());
		Factura factura1 = new Factura("4324242", LocalDate.now(), "deportivo", 40000, 1, cliente1);
		gestor.crearFactura(factura1);
		assertEquals(null, factura1, gestor.facturaMasCara());

	}

	// Dos facturas igual de caras//85800
	@Test
	void facturasIgualCaras() {

		Cliente cliente1 = new Cliente("juan", "tertertert", LocalDate.now());
		Factura factura1 = new Factura("423423423", LocalDate.now(), "deportivo", 40000, 1, cliente1);
		Factura factura2 = gestor.facturaMasCara();
		gestor.crearFactura(factura1);
		assertEquals(null, factura1, factura2);

	}

	
	// Calcular facturacion anual
	
	@Test
	void facturacionAnualTest() {

		double facturacion = gestor.calcularFacturacionAnual(2018);
		assertEquals(null, 2600, facturacion);

	}


	// Calcular facturacion nula
	@Test
	void facturacionNulaTest() {

		double facturacion = gestor.calcularFacturacionAnual(2000);
		assertEquals(null, 0, facturacion);

	}

	// Asignar cliente existente a factura
	@Test
	void asignarClienteExistenteFactura() {

		gestor.asignarClienteAFactura("2324224", "43434344");
		Factura factura = gestor.buscarFactura("2324224");
		Cliente cliente = gestor.buscarCliente("43434344");
		assertEquals(null, cliente, factura.getCliente());

	}

	// Asignar cliente a factura inexistente
	@Test
	void asignarClienteFacturaNoExiste() {

		gestor.asignarClienteAFactura("fdfdd", "dfsdsds");
		Factura factura = gestor.buscarFactura("2324224");
		assertNull(factura);

	}

	// Cantidad facturas pro cliente
	@Test
	void cantidadFacturas() {

		int cantidad = gestor.cantidadFacturasPorCliente("43434344");
		assertEquals(1, cantidad);

	}

	// Cantidad facturas cliente inexistente
	@Test
	void cantidadFacturasInexistentes() {

		int cantidad = gestor.cantidadFacturasPorCliente("ddddddddd");
		assertEquals(0, cantidad);

	}

	// Eliminar factura
	@Test
	void eliminarFactura() {

		gestor.eliminarFactura("2324224");
		assertEquals(1, gestor.getListaFacturas().size());

	}

	// Eliminar factura inexistente
	@Test
	void eliminarFacturaInexistente() {

		gestor.eliminarFactura("4343434");
		assertEquals(2, gestor.getListaFacturas().size());

	}

}


